package main.java;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Juego  extends JPanel {

		Personaje personaje = new Personaje();
		static ArrayList objetos = new ArrayList<Objeto>();
	
		public Juego() {
			addKeyListener(new KeyListener() {
				@Override
				public void keyTyped(KeyEvent e) {
				}

				@Override
				public void keyReleased(KeyEvent e) {
				}

				@Override
				public void keyPressed(KeyEvent e) {

				}
			});
			setFocusable(true);
		}
		
		private void recalculaPosiciones() {
		}
		
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			
			this.personaje.pintaPersonaje((Graphics2D) g);
			for (int i=0; i<objetos.size();i++) {
				((Objeto)this.objetos.get(i)).pinta(g);
			}
		}
		
		public void AnadeObstaculo() {
			this.objetos.add(new Objeto(300,50));
		}

		public static void main(String[] args) throws InterruptedException {
			JFrame frame = new JFrame("Titulo");
			Juego game = new Juego();
			frame.add(game);
			frame.setSize(700, 500);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			while (true) {
				game.recalculaPosiciones();
				game.repaint();
				Thread.sleep(10);
			}
		}
}