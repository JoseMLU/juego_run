package main.java;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
/**
 *Clase personaje que permite saber su posicion y creacion 
 *y con varios metodo de busqueda
 * @author JoseMoro
 */

public class Personaje{
	int y=0;
	int ya=0;
	
//	public void mover() {
//		if (y + ya > 0 && y + ya < game.getWidth())
//			y = y + ya;
//		if(y==1)
//			y=0;
//	}
	
	 /**
     * Pinta una imagen en una posici�n determinada.
     * @param g Objeto de tipo Graphics que se usara para pintar la clase.
     **/
	
	
	public void pintaPersonaje(Graphics2D g) {
		g.fillRect(y, 330, 10, 60);
	}	
	
	public void keyReleased(KeyEvent e) {
		ya = 0;
	}
	 /**
     * Metodo que se utiliza para mover al Personaje de arriba a Bajo 
     * Si el objeto pulsa dos veces a bajo devuelve un
     *  @return agachado
     * @param e Objeto de tipo Evento que se usara para mover el objeto .
     **/
	
	
	public boolean teclaPulsada(KeyEvent e) {
		boolean agachado=false;
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			agachado=true;
		}
		if (e.getKeyCode() == KeyEvent.VK_UP)
			ya = 10;
		
		return agachado;
	}	
	
	
	/**
	 * Metodo busqueda binaria
	 * @param dato  un numero entero que introducimos para saber que poscion esta o si no esta ese numero
	 * @param vector  una array estatico con 23 posiciones y diferentes numeros 
	 * @param n longitud del vector
	 * @param centro es el centro del vector
	 * @param inf limite inferior a 0
	 * @param sup limite superior (N-1)
	 * @return nos devulve la posicion del numero y si no existe nos devuelve -1
	 * 
	 */

	public static int busquedaBinaria(int dato){
		  int[]vector ={1,4,7,8,9,14,23,47,56,60,61,63,65,66,68,69,70,73,76,77,79,80,82};
		  int n = vector.length;
		  int centro,inf=0,sup=n-1;
		   while(inf<=sup){
		     centro=(sup+inf)/2;
		     if(vector[centro]==dato) return centro;
		     else if(dato < vector [centro] ){
		        sup=centro-1;
		     }
		     else {
		       inf=centro+1;
		     }
		   }
		   return -1;
		 }
	
}
