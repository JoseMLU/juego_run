package main.java;

import static org.junit.Assert.*;

import org.junit.Test;

public class ObjetoTest {

	@Test
	public void test_fibonacci_LimiteCero() {
		Objeto o = new Objeto(10, 10);
		
		String resultado = o.serie_fibonacci_solo_par(0);
		String esperado = "";
		
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void test_fibonacci_LimiteNegativo() {
		Objeto o = new Objeto(10, 10);
		
		String resultado = o.serie_fibonacci_solo_par(-5);
		String esperado = "";
		
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void test_fibonacci_LimitePositivo() {
		Objeto o = new Objeto(10, 10);
		
		String resultado = o.serie_fibonacci_solo_par(10);
		String esperado = "0, 2, 8, 34, ";
		
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void test_fibonacci_LimiteUno() {
		Objeto o = new Objeto(10, 10);
		
		String resultado = o.serie_fibonacci_solo_par(1);
		String esperado = "0, ";
		
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void test_fibonacci_LimiteDos() {
		Objeto o = new Objeto(10, 10);
		
		String resultado = o.serie_fibonacci_solo_par(2);
		String esperado = "0, ";
		
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void test_fibonacci_LimiteTres() {
		Objeto o = new Objeto(10, 10);
		
		String resultado = o.serie_fibonacci_solo_par(3);
		String esperado = "0, ";
		
		assertEquals(esperado, resultado);
	}

}
