package main.java;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
import java.util.Scanner;
/*
 * @author Jos� Moro Luque
 * @control de Test Busqueda Binaria 
 * @Test
*/

public class PersonajeTest {

	@Test
	public void PersonajeTest() {
		int dato = 10;

		System.out.println(Personaje.busquedaBinaria(dato));

	}

	@Test
	public void PersonajeTest1() {
		int dato = 4;
		int[] vector = { 1, 4, 7, 8, 9, 14, 23, 47, 56, 60, 61, 63, 65, 66, 68, 69, 70, 73, 76, 77, 79, 80, 82 };
		int n = 0;
		int cont;
		System.out.println(Personaje.busquedaBinaria(dato));

		if (vector[dato] == Personaje.busquedaBinaria(dato)) {
			cont = vector[dato];
			assertEquals(Personaje.busquedaBinaria(dato), Personaje.busquedaBinaria(cont));
		}
	}

	@Test
	public void PersonajeTest2() {
		int dato = 3;

		assertEquals(Personaje.busquedaBinaria(dato), -1);
	}

}


