package main.java;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

/**
 * Clase que permite definir un objeto con una posici�n
 * determinada, dimensiones, si es s�lido y una imagen.
 *
 * @author alejandro
 */
public class Objeto {
    
    public float posX = 0;
    public float posY = 0;
    public int width;
    public int height;
    public boolean solid = false;
    
    public BufferedImage image = null;
    
    /**
     * M�todo que construye un objeto indicando su posici�n.
     * @param posX - valor del eje X ingresado por el usuario.
     * @param posY - valor del eje Y ingresado por el usuario.
     */
    public Objeto(float posX, float posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public void update(float deltaTime) {
        //
    }
    
    /**
     * Pinta una imagen en una posici�n determinada.
     * Si no hay un imagen, el m�todo se detendr�.
     * 
     * @param g Objeto de tipo Graphics que se usar� 
     * para construir la imagen en la posici�n indicada.
     */
    public void pinta(Graphics g) {
        if(image == null) {
            return;
        }
        int realX = (int)posX;
        int realY = (int)posY;
           
        g.drawImage(image, realX, realY, width, height, null);
    }

    public boolean isSolid() {
        return solid;
    }   
    
    /**
     * Genera la serie fivonacci hasta el l�mite indicado y solo devuelve
     * los n�meros pares.
     * 
     * @param limite Cantidad de n�meros pertenecientes a la serie que ser�n generados.
     * 
     * @return Devuelve los n�meros pares dentro de la cantidad l�mite especificada.
     */
    public String serie_fibonacci_solo_par(int limite) {
    	String resultado = "";
        int n1 = 0;
        int n2 = 1;
        
        if(limite >= 1) 
            resultado += n1 + ", ";
 
        for(int i = 0; i<limite-2; i++){
            n2 = n1 + n2;
            n1 = n2 - n1;
            if (n2 % 2 == 0) { 
                resultado += n2 + ", ";
            }
        }
        return resultado;
    }
			
}

	
	
	
	
	
	

